---
title: 'Ready to <strong>make</strong> something?'
buttons:
    -
        text: 'Lets talk'
        url: /contact
        class: special
    -
        text: 'I''m thinking about it'
        url: /contact
menu: after_article
---

Get in touch to find out how Lazy Bear Records can help you realise your dreams.