---
title: 'If you want to distribute it, we can facilitate it'
buttons:
    -
        text: 'Find Out More'
        url: '#'
icons:
    -
        label: 'Feature 1'
        icon: file-audio-o
    -
        label: 'Feature 2'
        icon: music
    -
        label: 'Feature 3'
        icon: spotify
    -
        label: 'Feature 4'
        icon: rss
    -
        label: 'Feature 5'
        icon: apple
    -
        label: 'Feature 6'
        icon: rocket
---

Even if you're not looking at a physical release, Lazy Bear Records has you covered. After our first release; <strong>The Wretched</strong>, We're able to produce and distribute on all major platforms, including streaming services such as Spotify and bandcamp, physically on CD, Cassette, and USB.