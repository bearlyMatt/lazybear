---
title: 'Take a look at our <strong>Projects</strong>'
portfolio:
    -
        image: pic01.jpg
        image_link: 'https://lazybearrecordsuk.bandcamp.com'
        title: 'The Wretched'
        text: '*Audiodrama*: When your spaceship crashes, and your crewmates fall foul of the local fauna how do you survive? An hour-long science fiction narrative told through evolving audio logs.'
    -
        image: pic02.jpg
        image_link: '#'
        title: 'An Airport Terminal'
        text: 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
    -
        image: pic03.jpg
        image_link: '#'
        title: 'Hyperspace Travel'
        text: 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
    -
        image: pic04.jpg
        image_link: '#'
        title: 'And Another Train'
        text: 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
buttons:
    -
        text: 'See More'
        url: 'https://lazybearrecordsuk.bandcamp.com/'
---

