---
title: 'Lazy Bear Records'
buttons:
    -
        text: 'Tell me more'
        url: '#main'
menu: before_article
---

<p>An independent audio <strong>Micro Label</strong>,
    <br />
    From <strong>Ambient</strong> and <strong>Audiobooks</strong>,
    <br />
    To <strong>Experimental</strong> and <strong>Rock</strong>.
    <br />
    Based in <strong>Edinburgh</strong>, Scotland.
    <br />
</p>