<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/header.html.twig */
class __TwigTemplate_87d604a711864fd83e24239e0fb71049482115d6d48c48d145edea97f5e37c19 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<header id=\"header\" class=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", []), "header_class", []), "html", null, true);
        echo "\">
  <h1 id=\"logo\"><a href=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["base_url_absolute"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["site"] ?? null), "logo", []), "text1", []), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["site"] ?? null), "logo", []), "text2", []), "html", null, true);
        echo "</span></a></h1>
  <nav id=\"nav\">
    <ul>
      <li class=\"current\"><a href=\"";
        // line 5
        echo twig_escape_filter($this->env, ($context["base_url_absolute"] ?? null), "html", null, true);
        echo "\">Welcome</a></li>
      <li class=\"submenu\">
        <a href=\"\">Layouts</a>
        <ul>
          ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["pages"] ?? null), "children", []));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            // line 10
            echo "          ";
            if ($this->getAttribute($context["page"], "visible", [])) {
                // line 11
                echo "          ";
                $context["current_page"] = ((($this->getAttribute($context["page"], "active", []) || $this->getAttribute($context["page"], "activeChild", []))) ? ("current") : (""));
                // line 12
                echo "          <li class=\"";
                echo twig_escape_filter($this->env, ($context["current_page"] ?? null), "html", null, true);
                echo "\">
            <a href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "url", []), "html", null, true);
                echo "\">
              ";
                // line 14
                echo twig_escape_filter($this->env, $this->getAttribute($context["page"], "menu", []), "html", null, true);
                echo "
            </a>
          </li>
          ";
            }
            // line 18
            echo "          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["site"] ?? null), "menu", []));
        foreach ($context['_seq'] as $context["_key"] => $context["mitem"]) {
            // line 20
            echo "          <li>
            <a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["mitem"], "link", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["mitem"], "text", []), "html", null, true);
            echo "</a>
          </li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mitem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "          ";
        if ($this->getAttribute(($context["site"] ?? null), "submenu", [])) {
            // line 25
            echo "          <li class=\"submenu\">
            <a href=\"\">Submenu</a>
            <ul>
              ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["site"] ?? null), "submenu", []));
            foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                // line 29
                echo "              <li><a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["submenu"], "link", []), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["submenu"], "text", []), "html", null, true);
                echo "</a></li>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "            </ul>
          </li>
          ";
        }
        // line 34
        echo "        </ul>
      </li>

      ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["site"] ?? null), "buttons", []));
        foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
            // line 38
            echo "      <li><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["button"], "link", []), "html", null, true);
            echo "\" class=\"button special\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["button"], "text", []), "html", null, true);
            echo "</a></li>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "    </ul>
  </nav>
</header>
";
    }

    public function getTemplateName()
    {
        return "partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 40,  140 => 38,  136 => 37,  131 => 34,  126 => 31,  115 => 29,  111 => 28,  106 => 25,  103 => 24,  92 => 21,  89 => 20,  84 => 19,  78 => 18,  71 => 14,  67 => 13,  62 => 12,  59 => 11,  56 => 10,  52 => 9,  45 => 5,  35 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<header id=\"header\" class=\"{{ page.header.header_class }}\">
  <h1 id=\"logo\"><a href=\"{{ base_url_absolute }}\">{{ site.logo.text1 }} <span>{{ site.logo.text2 }}</span></a></h1>
  <nav id=\"nav\">
    <ul>
      <li class=\"current\"><a href=\"{{ base_url_absolute }}\">Welcome</a></li>
      <li class=\"submenu\">
        <a href=\"\">Layouts</a>
        <ul>
          {% for page in pages.children %}
          {% if page.visible %}
          {% set current_page = (page.active or page.activeChild) ? 'current' : '' %}
          <li class=\"{{ current_page }}\">
            <a href=\"{{ page.url }}\">
              {{ page.menu }}
            </a>
          </li>
          {% endif %}
          {% endfor %}
          {% for mitem in site.menu %}
          <li>
            <a href=\"{{ mitem.link }}\">{{ mitem.text }}</a>
          </li>
          {% endfor %}
          {% if site.submenu %}
          <li class=\"submenu\">
            <a href=\"\">Submenu</a>
            <ul>
              {% for submenu in site.submenu %}
              <li><a href=\"{{ submenu.link }}\">{{ submenu.text }}</a></li>
              {% endfor %}
            </ul>
          </li>
          {% endif %}
        </ul>
      </li>

      {% for button in site.buttons %}
      <li><a href=\"{{ button.link }}\" class=\"button special\">{{ button.text }}</a></li>
      {% endfor %}
    </ul>
  </nav>
</header>
", "partials/header.html.twig", "/var/www/lazyBear/user/themes/twenty/templates/partials/header.html.twig");
    }
}
