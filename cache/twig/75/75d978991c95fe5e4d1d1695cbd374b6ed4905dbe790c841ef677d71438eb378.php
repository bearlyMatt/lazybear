<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/sidebar_article.html.twig */
class __TwigTemplate_f7c44a2532e4a94227f22f0f5451c6f28a17951eab89ac229b699e4da31bf8f4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"content\">
    <section>
        ";
        // line 3
        if ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", []), "featured_image", [])) {
            // line 4
            echo "            <a href=\"#\" class=\"image featured\"><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "media", []), $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", []), "featured_image", []), [], "array"), "url", []), "html", null, true);
            echo "\" alt=\"\" /></a>
        ";
        }
        // line 6
        echo "        <header>
            <h3>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", []), "title", []), "html", null, true);
        echo "</h3>
        </header>
        ";
        // line 9
        echo ($context["content"] ?? null);
        echo "
    </section>
</div>
";
    }

    public function getTemplateName()
    {
        return "partials/sidebar_article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  45 => 7,  42 => 6,  36 => 4,  34 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"content\">
    <section>
        {% if page.header.featured_image %}
            <a href=\"#\" class=\"image featured\"><img src=\"{{ page.media[page.header.featured_image].url }}\" alt=\"\" /></a>
        {% endif %}
        <header>
            <h3>{{ page.header.title }}</h3>
        </header>
        {{ content|raw }}
    </section>
</div>
", "partials/sidebar_article.html.twig", "/var/www/lazyBear/user/themes/twenty/templates/partials/sidebar_article.html.twig");
    }
}
