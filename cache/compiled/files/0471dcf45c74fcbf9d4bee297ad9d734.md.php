<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_banner/banner.md',
    'modified' => 1622495018,
    'data' => [
        'header' => [
            'title' => 'Lazy Bear Records',
            'buttons' => [
                0 => [
                    'text' => 'Tell me more',
                    'url' => '#main'
                ]
            ],
            'menu' => 'before_article'
        ],
        'frontmatter' => 'title: \'Lazy Bear Records\'
buttons:
    -
        text: \'Tell me more\'
        url: \'#main\'
menu: before_article',
        'markdown' => '<p>An independent audio <strong>Micro Label</strong>,
    <br />
    From <strong>Ambient</strong> and <strong>Audiobooks</strong>,
    <br />
    To <strong>Experimental</strong> and <strong>Rock</strong>.
    <br />
    Based in <strong>Edinburgh</strong>, Scotland.
    <br />
</p>'
    ]
];
