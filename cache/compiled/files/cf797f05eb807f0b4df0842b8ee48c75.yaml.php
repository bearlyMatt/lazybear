<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/lazyBear/user/accounts/admin.yaml',
    'modified' => 1622493978,
    'data' => [
        'state' => 'enabled',
        'email' => 'matthewharazim@vivaldi.net',
        'fullname' => 'Matthew Harazim',
        'title' => 'Administrator',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$CiboD1/VvjroPGn1dlAiGu0QIHfkGhgDqd6ihhG.S8/fttJbbN.IS'
    ]
];
