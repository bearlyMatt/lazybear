<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_icons/icons.md',
    'modified' => 1622498063,
    'data' => [
        'header' => [
            'title' => 'If you want to distribute it, we can facilitate it',
            'buttons' => [
                0 => [
                    'text' => 'Find Out More',
                    'url' => '#'
                ]
            ],
            'icons' => [
                0 => [
                    'label' => 'Feature 1',
                    'icon' => 'file-audio-o'
                ],
                1 => [
                    'label' => 'Feature 2',
                    'icon' => 'music'
                ],
                2 => [
                    'label' => 'Feature 3',
                    'icon' => 'spotify'
                ],
                3 => [
                    'label' => 'Feature 4',
                    'icon' => 'rss'
                ],
                4 => [
                    'label' => 'Feature 5',
                    'icon' => 'apple'
                ],
                5 => [
                    'label' => 'Feature 6',
                    'icon' => 'rocket'
                ]
            ]
        ],
        'frontmatter' => 'title: \'If you want to distribute it, we can facilitate it\'
buttons:
    -
        text: \'Find Out More\'
        url: \'#\'
icons:
    -
        label: \'Feature 1\'
        icon: file-audio-o
    -
        label: \'Feature 2\'
        icon: music
    -
        label: \'Feature 3\'
        icon: spotify
    -
        label: \'Feature 4\'
        icon: rss
    -
        label: \'Feature 5\'
        icon: apple
    -
        label: \'Feature 6\'
        icon: rocket',
        'markdown' => 'Even if you\'re not looking at a physical release, Lazy Bear Records has you covered. After our first release; <strong>The Wretched</strong>, We\'re able to produce and distribute on all major platforms, including streaming services such as Spotify and bandcamp, physically on CD, Cassette, and USB.'
    ]
];
