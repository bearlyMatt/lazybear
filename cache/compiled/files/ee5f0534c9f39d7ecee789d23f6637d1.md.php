<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/03.rightsidebar/_features/features.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'features' => [
                0 => [
                    'icon' => NULL,
                    'title' => 'This is Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.',
                    'buttons' => [
                        0 => [
                            'text' => 'Learn More',
                            'url' => '#main'
                        ]
                    ]
                ],
                1 => [
                    'icon' => NULL,
                    'title' => 'Also Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.',
                    'buttons' => [
                        0 => [
                            'text' => 'Learn More',
                            'url' => '#main'
                        ]
                    ]
                ],
                2 => [
                    'icon' => NULL,
                    'title' => 'Probably Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.',
                    'buttons' => [
                        0 => [
                            'text' => 'Learn More',
                            'url' => '#main'
                        ]
                    ]
                ]
            ]
        ],
        'frontmatter' => 'features:
    - icon:
      title: This is Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.
      buttons:
        - text: Learn More
          url: \'#main\'
    - icon:
      title: Also Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.
      buttons:
        - text: Learn More
          url: \'#main\'
    - icon:
      title: Probably Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.
      buttons:
        - text: Learn More
          url: \'#main\'    ',
        'markdown' => ''
    ]
];
