<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_portfolio/portfolio.md',
    'modified' => 1622496209,
    'data' => [
        'header' => [
            'title' => 'Take a look at our <strong>Projects</strong>',
            'portfolio' => [
                0 => [
                    'image' => 'pic01.jpg',
                    'image_link' => 'https://lazybearrecordsuk.bandcamp.com',
                    'title' => 'The Wretched',
                    'text' => '*Audiodrama*: When your spaceship crashes, and your crewmates fall foul of the local fauna how do you survive? An hour-long science fiction narrative told through evolving audio logs.'
                ],
                1 => [
                    'image' => 'pic02.jpg',
                    'image_link' => '#',
                    'title' => 'An Airport Terminal',
                    'text' => 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
                ],
                2 => [
                    'image' => 'pic03.jpg',
                    'image_link' => '#',
                    'title' => 'Hyperspace Travel',
                    'text' => 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
                ],
                3 => [
                    'image' => 'pic04.jpg',
                    'image_link' => '#',
                    'title' => 'And Another Train',
                    'text' => 'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.'
                ]
            ],
            'buttons' => [
                0 => [
                    'text' => 'See More',
                    'url' => 'https://lazybearrecordsuk.bandcamp.com/'
                ]
            ]
        ],
        'frontmatter' => 'title: \'Take a look at our <strong>Projects</strong>\'
portfolio:
    -
        image: pic01.jpg
        image_link: \'https://lazybearrecordsuk.bandcamp.com\'
        title: \'The Wretched\'
        text: \'*Audiodrama*: When your spaceship crashes, and your crewmates fall foul of the local fauna how do you survive? An hour-long science fiction narrative told through evolving audio logs.\'
    -
        image: pic02.jpg
        image_link: \'#\'
        title: \'An Airport Terminal\'
        text: \'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.\'
    -
        image: pic03.jpg
        image_link: \'#\'
        title: \'Hyperspace Travel\'
        text: \'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.\'
    -
        image: pic04.jpg
        image_link: \'#\'
        title: \'And Another Train\'
        text: \'Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.\'
buttons:
    -
        text: \'See More\'
        url: \'https://lazybearrecordsuk.bandcamp.com/\'',
        'markdown' => ''
    ]
];
