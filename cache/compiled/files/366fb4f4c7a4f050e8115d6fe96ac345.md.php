<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/modular.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'title' => 'Twenty',
            'menu' => 'Home',
            'onpage_menu' => true,
            'body_class' => 'index',
            'header_class' => 'alt',
            'content' => [
                'items' => '@self.modular',
                'order' => [
                    'by' => 'default',
                    'dir' => 'asc',
                    'custom' => [
                        0 => '_banner',
                        1 => '_header',
                        2 => '_icons',
                        3 => '_features'
                    ]
                ]
            ]
        ],
        'frontmatter' => 'title: Twenty
menu: Home
onpage_menu: true
body_class: index
header_class: alt
content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _banner
            - _header
            - _icons
            - _features',
        'markdown' => ''
    ]
];
