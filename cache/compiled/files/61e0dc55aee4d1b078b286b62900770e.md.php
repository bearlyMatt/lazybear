<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_features/features.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'features' => [
                0 => [
                    'icon' => 'check',
                    'title' => 'This is Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.'
                ],
                1 => [
                    'icon' => 'check',
                    'title' => 'Also Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.'
                ],
                2 => [
                    'icon' => 'check',
                    'title' => 'Probably Something',
                    'text' => 'Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.'
                ]
            ]
        ],
        'frontmatter' => 'features:
    - icon: check
      title: This is Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.
    - icon: check
      title: Also Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.  
    - icon: check
      title: Probably Something
      text: Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.    ',
        'markdown' => ''
    ]
];
