<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/lazyBear/user/config/site.yaml',
    'modified' => 1622494086,
    'data' => [
        'title' => 'Lazy Bear Records',
        'default_lang' => 'en',
        'author' => [
            'name' => 'Matthew Harazim',
            'email' => 'matthewharazim@vivaldi.net'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'Lazy Bear Records is an independent audio micro-label publishing music from ambient to audiobooks and bebop to reggae.'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'redirects' => NULL,
        'routes' => NULL,
        'blog' => [
            'route' => '/blog'
        ],
        'email' => 'your-email@domain.com',
        'description' => 'Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.',
        'social' => [
            0 => [
                'label' => 'Twitter',
                'url' => '#',
                'icon' => 'twitter'
            ],
            1 => [
                'label' => 'Facebook',
                'url' => '#',
                'icon' => 'facebook'
            ],
            2 => [
                'label' => 'Google+',
                'url' => '#',
                'icon' => 'google-plus'
            ],
            3 => [
                'label' => 'Github',
                'url' => '#',
                'icon' => 'github'
            ],
            4 => [
                'label' => 'Dribbble',
                'url' => '#',
                'icon' => 'dribbble'
            ]
        ],
        'copyright' => [
            'line1' => 'Untitled',
            'line2' => 'Design:',
            'url' => 'http://html5up.net',
            'url_label' => 'HTML5 UP'
        ],
        'logo' => [
            'text1' => 'Twenty',
            'text2' => 'by HTML5 UP'
        ],
        'submenu' => [
            0 => [
                'text' => 'Dolore Sed',
                'link' => '#'
            ],
            1 => [
                'text' => 'Consequat',
                'link' => '#'
            ],
            2 => [
                'text' => 'Lorem Magna',
                'link' => '#'
            ],
            3 => [
                'text' => 'Sed Magna',
                'link' => '#'
            ],
            4 => [
                'text' => 'Ipsum Nisl',
                'link' => '#'
            ]
        ],
        'buttons' => [
            0 => [
                'text' => 'Sign Up',
                'link' => '#'
            ]
        ]
    ]
];
