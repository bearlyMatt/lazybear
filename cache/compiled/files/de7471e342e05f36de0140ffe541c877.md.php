<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/03.rightsidebar/modular.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'title' => 'Right Sidebar',
            'subtitle' => 'Behold the <strong>Right Sidebar</strong>',
            'description' => 'Where things on the right ... accompany that on the left.',
            'body_class' => 'right-sidebar',
            'header_class' => 'skel-layers-fixed',
            'icon' => 'tablet',
            'onpage_menu' => true,
            'content' => [
                'items' => '@self.modular',
                'order' => [
                    'by' => 'default',
                    'dir' => 'asc',
                    'custom' => [
                        0 => '_content'
                    ]
                ]
            ]
        ],
        'frontmatter' => 'title: Right Sidebar
subtitle: "Behold the <strong>Right Sidebar</strong>"
description: "Where things on the right ... accompany that on the left."
body_class: right-sidebar
header_class: skel-layers-fixed
icon: tablet
onpage_menu: true
content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _content',
        'markdown' => ''
    ]
];
