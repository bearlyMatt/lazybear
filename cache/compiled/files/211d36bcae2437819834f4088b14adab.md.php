<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/02.leftsidebar/modular.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'title' => 'Left Sidebar',
            'subtitle' => 'Behold the <strong>Left Sidebar</strong>',
            'description' => 'Where things on the left ... accompany that on the right.',
            'body_class' => 'left-sidebar',
            'header_class' => 'skel-layers-fixed',
            'icon' => 'laptop',
            'onpage_menu' => true,
            'content' => [
                'items' => '@self.modular',
                'order' => [
                    'by' => 'default',
                    'dir' => 'asc',
                    'custom' => [
                        0 => '_content'
                    ]
                ]
            ]
        ],
        'frontmatter' => 'title: Left Sidebar
subtitle: "Behold the <strong>Left Sidebar</strong>"
description: "Where things on the left ... accompany that on the right."
body_class: left-sidebar
header_class: skel-layers-fixed
icon: laptop
onpage_menu: true
content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _content',
        'markdown' => ''
    ]
];
