<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_bottom/bottom.md',
    'modified' => 1622552349,
    'data' => [
        'header' => [
            'title' => 'Ready to <strong>make</strong> something?',
            'buttons' => [
                0 => [
                    'text' => 'Lets talk',
                    'url' => '/contact',
                    'class' => 'special'
                ],
                1 => [
                    'text' => 'I\'m thinking about it',
                    'url' => '/contact'
                ]
            ],
            'menu' => 'after_article'
        ],
        'frontmatter' => 'title: \'Ready to <strong>make</strong> something?\'
buttons:
    -
        text: \'Lets talk\'
        url: /contact
        class: special
    -
        text: \'I\'\'m thinking about it\'
        url: /contact
menu: after_article',
        'markdown' => 'Get in touch to find out how Lazy Bear Records can help you realise your dreams.'
    ]
];
