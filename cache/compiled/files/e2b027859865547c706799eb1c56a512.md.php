<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/04.nosidebar/modular.md',
    'modified' => 1619815198,
    'data' => [
        'header' => [
            'title' => 'No Sidebar',
            'subtitle' => 'And finally there\'s <strong>No Sidebar</strong>',
            'description' => 'Where that in the center faces the nameless horrors alone.',
            'body_class' => 'no-sidebar',
            'header_class' => 'skel-layers-fixed',
            'icon' => 'mobile',
            'onpage_menu' => true,
            'content' => [
                'items' => '@self.modular',
                'order' => [
                    'by' => 'default',
                    'dir' => 'asc',
                    'custom' => [
                        0 => '_content'
                    ]
                ]
            ]
        ],
        'frontmatter' => 'title: No Sidebar
subtitle: "And finally there\'s <strong>No Sidebar</strong>"
description: "Where that in the center faces the nameless horrors alone."
body_class: no-sidebar
header_class: skel-layers-fixed
icon: mobile
onpage_menu: true
content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _content',
        'markdown' => ''
    ]
];
