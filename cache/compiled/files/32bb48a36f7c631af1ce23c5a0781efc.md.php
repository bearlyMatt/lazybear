<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/lazyBear/user/pages/01.home/_header/header.md',
    'modified' => 1622497452,
    'data' => [
        'header' => [
            'title' => 'A label <strong>by</strong> introverts <strong>for</strong> introverts',
            'icon' => 'heart-o'
        ],
        'frontmatter' => 'title: \'A label <strong>by</strong> introverts <strong>for</strong> introverts\'
icon: heart-o',
        'markdown' => '<p>
Specialising in <strong>limited-run</strong> physical releases, Lazy Bear Records puts the <strong>artist first</strong>. Coming from academic backgrounds, Lazy Bear Records knows how great it feels to make that special EP, LP, or single, but not have enough capital to make it a tangible reality. Between helping to fund physical releases, designing cover art, and promoting our artists, Lazy Bear Records helps to maximise exposure on the <strong>experimental</strong> festival scene; having successfully placed artists in <a href="https://sound-scotland.co.uk"> <strong>Sound Festival</strong></a> (Aberdeen), <a href="http://sonada.org/sonada_main.html"> <strong>SonADA</strong></a> (Aberdeen), <a href="https://www.soundslikethis.co.uk"><strong>Sounds Like THIS</strong></a> (Leeds), and <a href="http://www.dialogues-festival.org"><strong>Dialogues</strong></a> (Edinburgh).
</p>'
    ]
];
